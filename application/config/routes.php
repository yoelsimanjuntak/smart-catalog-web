<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'site/user/login';
$route['404_override'] = 'home/_404';
$route['translate_uri_dashes'] = TRUE;

$route['user/(:any)'] = 'site/user/$1';
$route['user/(:any)/(:any)'] = 'site/user/$1/$2';

$route['setting/(:any)'] = 'site/setting/$1';
$route['setting/(:any)/(:any)'] = 'site/setting/$1/$2';

$route['ajax/(:any)'] = 'sakip/ajax/$1';
$route['ajax/(:any)/(:any)'] = 'sakip/ajax/$1/$2';

$route['dpa/(:any)'] = 'sakip/dpa/$1';
$route['dpa/(:any)/(:any)'] = 'sakip/dpa/$1/$2';

$route['mbid/(:any)'] = 'sakip/mbid/$1';
$route['mbid/(:any)/(:any)'] = 'sakip/mbid/$1/$2';

$route['mopd/(:any)'] = 'sakip/mopd/$1';
$route['mopd/(:any)/(:any)'] = 'sakip/mopd/$1/$2';
$route['mopd/(:any)/(:any)/(:any)'] = 'sakip/mopd/$1/$2/$3';
$route['mopd/(:any)/(:any)/(:any)/(:any)'] = 'sakip/mopd/$1/$2/$3/$4';

$route['mpemda/(:any)'] = 'sakip/mpemda/$1';
$route['mpemda/(:any)/(:any)'] = 'sakip/mpemda/$1/$2';
$route['mpemda/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'sakip/mpemda/$1/$2/$3/$4/$5/$6/$7/$8/$9';

$route['msubbid/(:any)'] = 'sakip/msubbid/$1';
$route['msubbid/(:any)/(:any)'] = 'sakip/msubbid/$1/$2';

$route['rencana-aksi/(:any)'] = 'sakip/rencana-aksi/$1';
$route['rencana-aksi/(:any)/(:any)'] = 'sakip/rencana-aksi/$1/$2';

$route['perubahan-rencana-aksi/(:any)'] = 'sakip/perubahan-rencana-aksi/$1';
$route['perubahan-rencana-aksi/(:any)/(:any)'] = 'sakip/perubahan-rencana-aksi/$1/$2';

$route['report/(:any)'] = 'sakip/report/$1';
$route['report/(:any)/(:any)'] = 'sakip/report/$1/$2';

$route['home/(:any)'] = 'site/home/$1';
$route['home/(:any)/(:any)'] = 'site/home/$1/$2';
