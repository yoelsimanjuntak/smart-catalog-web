<?php
$rpkg = $this->db
->order_by(COL_STATUSSEQ)
->get(TBL_ORDERSTATUS)
->result_array();
?>
<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
#datalist tbody th, #datalist tbody td {
  vertical-align: middle;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>NOMOR</th>
                    <th>PEMESAN</th>
                    <th>STATUS</th>
                    <th>TOTAL</th>
                    <th>DIBUAT PADA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterPackage" style="width: 200px">
    <?=GetCombobox("select * from orderstatus order by StatusSeq", COL_UNIQ, COL_STATUSNAME, null, true, false, '-- SEMUA STATUS --')?>
  </select>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var orderdef = [];
  var coldefs = [];
  var cols = [];

  orderdef = [[ 5, "desc" ]];
  coldefs = [
    {"targets":[0], "className":'nowrap text-center'},
    {"targets":[4,5], "className":'nowrap dt-body-right'}
  ];
  cols = [
    {"orderable": false,"width": "50px"},
    {"orderable": true,"width": "10px"},
    {"orderable": true},
    {"orderable": true,"width": "10px"},
    {"orderable": false},
    {"orderable": true,"width": "10px"}
  ];

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/order/index-load/')?>",
      "type": 'POST',
      "data": function(data){
        data.filterPackage = $('[name=filterPackage]', $('.filtering')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": orderdef,
    "columnDefs": coldefs,
    "columns": cols,
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });
});
</script>
