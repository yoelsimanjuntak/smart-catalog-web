<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-default">
          <div class="card-header">
            <a href="<?=site_url('site/order/index')?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group row">
                  <label class="control-label col-sm-3">Status</label>
                  <div class="col-sm-8">
                    <select name="<?=COL_ORDERSTATUS?>" class="form-control" required>
                      <?=GetCombobox("SELECT * FROM orderstatus ORDER BY StatusSeq", COL_STATUSNAME, COL_STATUSNAME, (!empty($data[COL_ORDERSTATUS]) ? $data[COL_ORDERSTATUS] : null))?>
                    </select>
                  </div>
                </div>
                <!--<div class="form-group row">
                  <label class="control-label col-sm-3">Total</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control uang text-right" name="<?=COL_ORDERTOTAL?>" value="<?=!empty($data[COL_ORDERTOTAL]) ? $data[COL_ORDERTOTAL] : ''?>" placeholder="Total" disabled />
                  </div>
                </div>-->
                <div class="form-group row">
                  <label class="control-label col-sm-3">Pemesan</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="<?=COL_ORDERNAME?>" value="<?=!empty($data[COL_ORDERNAME]) ? $data[COL_ORDERNAME] : ''?>" placeholder="Nama Pemesan"  disabled/>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">No. HP</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="<?=COL_ORDERPHONE?>" value="<?=!empty($data[COL_ORDERPHONE]) ? $data[COL_ORDERPHONE] : ''?>"  disabled/>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">Alamat</label>
                  <div class="col-sm-8">
                    <textarea class="form-control" disabled><?=!empty($data[COL_ORDERADDRESS]) ? $data[COL_ORDERADDRESS] : ''?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">Catatan</label>
                  <div class="col-sm-8">
                    <textarea class="form-control" disabled><?=!empty($data[COL_ORDERREMARKS]) ? $data[COL_ORDERREMARKS] : ''?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <h6 style="text-decoration: underline">DAFTAR PRODUK</h6>
                <?php
                $arrItem = array();
                if(!empty($data) && !empty($data[COL_ORDERITEMS])) {
                  $arrItem = json_decode($data[COL_ORDERITEMS]);
                }
                ?>
                <table class="table table-bordered">
                  <tr>
                    <th style="width: 10px; white-space: nowrap">NO.</th>
                    <th>NAMA</th>
                    <th>JLH.</th>
                    <th style="width: 50px; white-space: nowrap;">HARGA</th>
                    <th style="width: 50px; white-space: nowrap;">SUB TOTAL</th>
                  </tr>
                  <?php
                  $no = 1;
                  foreach($arrItem as $i) {
                    ?>
                    <tr>
                      <td style="width: 10px; white-space: nowrap; text-align: right"><?=$no?></td>
                      <td><?=$i->PostTitle?></td>
                      <td class="text-right"><?=$i->Qty?></td>
                      <td class="text-right" style="width: 50px; white-space: nowrap;"><?=number_format($i->Price)?></td>
                      <td class="text-right" style="width: 50px; white-space: nowrap;"><?=number_format($i->Price*$i->Qty)?></td>
                    </tr>
                    <?php
                    $no++;
                  }
                  ?>
                  <tr>
                    <th colspan="4" class="text-right font-weight-bold">TOTAL</th>
                    <th class="text-right" style="width: 50px; white-space: nowrap;">
                      <?=!empty($data[COL_ORDERTOTAL]) ? number_format($data[COL_ORDERTOTAL]) : ''?>
                    </th>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
