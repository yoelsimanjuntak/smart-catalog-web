<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login | <?= $this->setting_web_name ?></title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontawesome-pro/web/css/all.min.css" />
    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
    <!-- jQuery -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="<?=base_url()?>assets/js/jquery.particleground.js"></script>
    <style>
        .adsbox{
            background: none repeat scroll 0 0 #f5f5f5;
            border-radius: 10px;
            box-shadow: 0 0 5px 1px rgba(50, 50, 50, 0.2);
            margin: 20px auto 0;
            padding: 15px;
            border: 1px solid #caced3;
            height: 145px;
        }
        html, body {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        #particles {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        #intro {
            position: absolute;
            left: 0;
            top: 50%;
            padding: 0 20px;
            width: 100%;
            #text-align: center;
        }
    </style>
    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
</head>
<!-- Preloader Style -->
<style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>') center no-repeat #fff;
    }
    .login-page::after {
      /*background: url(http://localhost/hh-bmkg//assets/media/image/footer-map-bg.png) no-repeat scroll center center / 75% auto;*/
      background: url('<?=base_url()?>assets/media/image/footer-map-bg.png');
      content: "";
      height: 100%;
      left: 0;
      opacity: 0.1;
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
      opacity: 0.1;
      background-color: #ffeb3b;
    }
</style>
<!-- /.preloader style -->

<!-- Preloader Script -->
<script>
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>
<!-- /.preloader script -->
<body class="hold-transition login-page" style="background: #277BC0">

<!-- preloader -->
<div class="se-pre-con"></div>
<!-- /.preloader -->

<div id="particles">
  <div id="intro">
    <div class="login-box">
        <div class="card" style="background: #fff">
            <div class="card-body login-card-body" style="background: none">
                <div class="login-logo">
                    <img class="user-image mb-3" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" style="width: 60px; height: 60px" alt="Logo"><br  />
                    <a href="<?=site_url()?>">
                        <h5 class="mb-0 font-weight-light">CONTENT MANAGEMENT SYSTEM</h5>
                        <p class="font-weight-bold" style="font-size: 10pt;"><?=nl2br($this->setting_web_desc)?></p>
                    </a>
                </div>

                <?php  if($this->input->get('msg') == 'notmatch'){ ?>
                  <div class="callout callout-danger">
                    <span class="text-danger"><i class="fad fa-key"></i>&nbsp;&nbsp;Username / password tidak tepat.</span>
                  </div>
                <?php } ?>

                <?php  if($this->input->get('msg') == 'suspend'){ ?>
                  <div class="callout callout-danger">
                    <span class="text-danger"><i class="fad fa-ban"></i>&nbsp;&nbsp;Akun anda disuspend.</span>
                  </div>
                <?php } ?>

                <?php  if($this->input->get('msg') == 'denied'){ ?>
                  <div class="callout callout-danger">
                    <span class="text-danger"><i class="fad fa-ban"></i>&nbsp;&nbsp;Anda tidak memiliki hak akses.</span>
                  </div>
                <?php } ?>

                <?php  if($this->input->get('msg') == 'captcha'){ ?>
                  <div class="callout callout-danger">
                    <span class="text-danger"><i class="fad fa-ban"></i>&nbsp;&nbsp;Captcha tidak sesuai.</span>
                  </div>
                <?php } ?>

                <?= form_open(current_url(),array('id'=>'validate')) ?>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="<?=COL_USERNAME?>" placeholder="Username" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fad fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fad fa-key"></span>
                        </div>
                    </div>
                </div>

                  <div class="footer" style="text-align: right;">
                  <a href="<?=site_url('site/home')?>" class="btn btn-default"><i class="far fa-home"></i>&nbsp;BERANDA</a>
                  <button type="submit" class="btn btn-success">MASUK&nbsp;<i class="far fa-sign-in"></i></button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.motio.min.js"></script>
<script>
    $(document).ready(function() {
        $('#particles').particleground({
            dotColor: '#FFF4CF',
            lineColor: '#FFB200'
        });
        $('#intro').css({
            'margin-top': -($('#intro').height() / 2)
        });
    });
</script>
</body>
</html>
