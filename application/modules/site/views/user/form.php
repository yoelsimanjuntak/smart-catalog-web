<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small class="text-sm"> Form</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('user/index')?>"> Pengguna</a></li>
          <li class="breadcrumb-item active"><?=$edit?'Ubah':'Tambah'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-primary">
          <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'userForm','class'=>'form-horizontal'))?>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12">
                  <?php
                  if ($this->input->get('error') == 1) {
                      ?>
                    <div class="callout callout-danger">
                      <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Data gagal disimpan, silahkan coba kembali.</span>
                    </div>
                    <?php
                  }
                  if (validation_errors()) {
                      ?>
                    <div class="callout callout-danger">
                      <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=validation_errors()?></span>
                    </div>
                    <?php
                  }
                  if (isset($err)) {
                      ?>
                    <div class="callout callout-danger">
                      <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$err['code'].' : '.$err['message']?></span>
                    </div>
                    <?php
                  }
                  if (!empty($upload_errors)) {
                      ?>
                    <div class="callout callout-danger">
                      <span class="text-danger"><?=$upload_errors?></span>
                    </div>
                    <?php
                  }
                  ?>
                </div>
                <div class="clearfix"></div>
                  <div class="col-sm-6">
                      <div class="form-group row">
                          <label class="control-label col-sm-4">Username</label>
                          <div class="col-sm-8">
                              <input type="text" class="form-control" name="<?=COL_USERNAME?>" value="<?= $edit ? $data[COL_USERNAME] : ""?>" <?=$edit?"disabled":""?> placeholder="Username" required>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label class="control-label col-sm-4">Email</label>
                          <div class="col-sm-8">
                              <input type="email" class="form-control" name="<?=COL_EMAIL?>" value="<?= $edit ? $data[COL_EMAIL] : ""?>" placeholder="Email" required>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label class="control-label col-sm-4">Nama</label>
                          <div class="col-sm-8">
                              <input type="text" class="form-control" name="<?=COL_NAME?>" value="<?= $edit ? $data[COL_NAME] : ""?>" placeholder="Nama Pengguna" required>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <?php
                    if(!$edit) {
                        ?>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="*****" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Confirm Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="RepeatPassword" placeholder="*****" >
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                      <div class="form-group row">
                          <label class="control-label col-sm-4">Role</label>
                          <div class="col-sm-8">
                              <select name="<?=COL_ROLEID?>" class="form-control" required>
                                  <option value="">Select Role</option>
                                  <?=GetCombobox("SELECT * FROM _roles", COL_ROLEID, COL_ROLENAME, (!empty($data[COL_ROLEID]) ? $data[COL_ROLEID] : null))?>
                              </select>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="card-footer text-center">
              <a href="<?=site_url('user/index')?>" class="btn btn-default"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
              <button type="submit" class="btn btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
            </div>
            <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Browse</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              ...
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="browseBid" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Browse</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="browseSubBid" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Browse</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('.modal').on('hidden.bs.modal', function (event) {
  $(this).find(".modal-body").empty();
});

$('.btn-browse-del').on('click', function (event) {
  var dis = $(this);
  dis.closest('.form-group').find('input').val('');
});


    $('#browseOPD').on('show.bs.modal', function (event) {
        var modalBody = $(".modal-body", $("#browseOPD"));
        $(this).removeData('bs.modal');
        modalBody.html("<p style='font-style: italic'>Loading..</p>");
        modalBody.load("<?=site_url("sakip/ajax/browse-opd")?>", function () {
            $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                var kdSub = $(this).val().split('|');
                $("[name=Kd_Urusan]").val(kdSub[0]);
                $("[name=Kd_Bidang]").val(kdSub[1]);
                $("[name=Kd_Unit]").val(kdSub[2]);
                $("[name=Kd_Sub]").val(kdSub[3]);
            });
            $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                $("[name=text-opd]").val($(this).val());
            });
        });
    });
    $('#browseBid').on('show.bs.modal', function (event) {
        var modalBody = $(".modal-body", $("#browseBid"));
        $(this).removeData('bs.modal');
        var kdUrusan = $("[name=Kd_Urusan]").val();
        var kdBidang = $("[name=Kd_Bidang]").val();
        var kdUnit = $("[name=Kd_Unit]").val();
        var kdSub = $("[name=Kd_Sub]").val();

        if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
            modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
            return;
        }

        modalBody.html("<p style='font-style: italic'>Loading..</p>");
        modalBody.load("<?=site_url("sakip/ajax/browse-bid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
            $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                $("[name=Kd_Bid]").val($(this).val());
            });
            $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                $("[name=text-bid]").val($(this).val());
            });
        });
    });
    $('#browseSubBid').on('show.bs.modal', function (event) {
        var modalBody = $(".modal-body", $("#browseSubBid"));
        $(this).removeData('bs.modal');

        var kdUrusan = $("[name=Kd_Urusan]").val();
        var kdBidang = $("[name=Kd_Bidang]").val();
        var kdUnit = $("[name=Kd_Unit]").val();
        var kdSub = $("[name=Kd_Sub]").val();
        var kdBid = $("[name=Kd_Bid]").val();

        if(!kdUrusan || !kdBidang || !kdUnit || !kdSub || !kdBid) {
            modalBody.html("<p style='font-style: italic'>Silakan pilih Bidang OPD terlebih dahulu!</p>");
            return;
        }

        modalBody.html("<p style='font-style: italic'>Loading..</p>");
        modalBody.load("<?=site_url("sakip/ajax/browse-subbid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub+"&Kd_Bid="+kdBid, function () {
            $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                $("[name=Kd_Subbid]").val($(this).val());
            });
            $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                $("[name=text-subbid]").val($(this).val());
            });
        });
    });
</script>
