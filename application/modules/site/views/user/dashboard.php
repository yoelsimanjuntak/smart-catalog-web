<?php
$ruser = GetLoggedUser();

 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?=$title?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <div class="card card-outline card-orange">
          <div class="card-header">
            <h5 class="card-title">PROFIL USAHA</h5>
          </div>
          <?=form_open_multipart(site_url('site/setting/change'), array('role'=>'form','id'=>'form-setting-org','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-sm-12">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="SETTING_ORG_NAME" value="<?=GetSetting('SETTING_ORG_NAME')?>" required />
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-12">
                  <label>Alamat</label>
                  <textarea class="form-control" name="SETTING_ORG_ADDRESS" rows="5" required><?=GetSetting('SETTING_ORG_ADDRESS')?></textarea>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>Telepon</label>
                  <input type="text" class="form-control" name="SETTING_ORG_PHONE" value="<?=GetSetting('SETTING_ORG_PHONE')?>" required />
                </div>
                <!--<div class="col-sm-6">
                  <label>Fax</label>
                  <input type="text" class="form-control" name="SETTING_ORG_FAX" value="<?=GetSetting('SETTING_ORG_FAX')?>" required />
                </div>-->
                <div class="col-sm-6">
                  <label>Email</label>
                  <input type="text" class="form-control" name="SETTING_ORG_MAIL" value="<?=GetSetting('SETTING_ORG_MAIL')?>" required />
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-12">
                  <label>Info Rekening</label>
                  <textarea class="form-control" name="SETTING_ORG_FAX" rows="5" required><?=GetSetting('SETTING_ORG_FAX')?></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-primary pull-right">SIMPAN&nbsp;<i class="far fa-arrow-circle-right"></i></button>
          </div>
          <?=form_close()?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-orange">
          <div class="card-header">
            <h5 class="card-title">PENGATURAN WEBSITE</h5>
          </div>
          <?=form_open_multipart(site_url('site/setting/change'), array('role'=>'form','id'=>'form-setting-web','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="form-group row">
              <label  class="control-label col-sm-3">Judul</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="SETTING_WEB_NAME" value="<?=GetSetting('SETTING_WEB_NAME')?>" required />
              </div>
            </div>
            <div class="form-group row">
              <label  class="control-label col-sm-3">Deskripsi</label>
              <div class="col-sm-9">
                <textarea class="form-control" name="SETTING_WEB_DESC" rows="5" required><?=GetSetting('SETTING_WEB_DESC')?></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label  class="control-label col-sm-3">Versi</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="SETTING_WEB_VERSION" value="<?=GetSetting('SETTING_WEB_VERSION')?>" required />
              </div>
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-primary pull-right">SIMPAN&nbsp;<i class="far fa-arrow-circle-right"></i></button>
          </div>
          <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-setting-org').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
  $('#form-setting-web').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
