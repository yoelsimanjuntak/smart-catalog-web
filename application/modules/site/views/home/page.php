<?php
$rheader = $this->db
->where(COL_ISHEADER, 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();

$postContent = $data[COL_POSTCONTENT];
$rimg = $this->db
->where(COL_ISHEADER.' != ', 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();
foreach($rimg as $img) {
  if(!empty($img[COL_IMGSHORTCODE])) {
    $postContent = str_replace($img[COL_IMGSHORTCODE], '<img src="'.MY_UPLOADURL.$img[COL_IMGPATH].'" style="max-width: 100%" /><p style="margin-top: 10px !important; font-size: 10px; font-style:italic; line-height: 1.5 !important">'.$img[COL_IMGDESC].'</p>', $postContent);
  }
}

$arrTags = array();
if(!empty($data[COL_POSTMETATAGS])) {
  $arrTags = explode(",", $data[COL_POSTMETATAGS]);
}

$txtShareWA = 'Jangan lewatkan update berita dan informasi terbaru dari '.$this->setting_web_name.' | '.urlencode(current_url());
?>
<section class="news-single section" style="background: #f9f9f9 !important; padding-top: 30px !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-12">
        <div class="row">
          <div class="col-12">
            <div class="single-main">
              <!-- News Head -->
              <div class="news-head">
                <?php
                if(!empty($rheader)) {
                  if(count($rheader) > 1) {
                    ?>
                    <div class="row mb-2">
                      <?php
                      foreach($rheader as $f) {
                        if(strpos(mime_content_type(MY_UPLOADPATH.$f[COL_IMGPATH]), 'image') !== false) {
                          ?>
                          <div class="col-12 col-sm-6 col-md-6 d-flex align-items-stretch p-2">
                            <div href="<?=MY_UPLOADURL.$f[COL_IMGPATH]?>"
                            data-toggle="lightbox"
                            data-title="<?=$data[COL_POSTTITLE]?>"
                            data-gallery="gallery"
                            style="background: url('<?=MY_UPLOADURL.$f[COL_IMGPATH]?>');
                                    background-size: cover;
                                    background-repeat: no-repeat;
                                    background-position: center;
                                    width: 100%;
                                    min-height: 300px;
                                    cursor: pointer;">
                            </div>
                          </div>
                          <?php
                        } else {
                          ?>
                          <div class="col-12 col-sm-12 col-md-12 mb-3 d-flex align-items-stretch">
                            <embed src="<?=MY_UPLOADURL.$f[COL_IMGPATH]?>" width="100%" height="600" />
                          </div>
                          <?php
                        }
                        ?>
                      <?php
                      }
                      ?>
                    </div>
                    <?php
                  } else {
                    ?>
                    <div class="row mb-2">
                      <div class="col-12 text-center">
                        <div href="<?=MY_UPLOADURL.$rheader[0][COL_IMGPATH]?>"
                        data-toggle="lightbox"
                        data-title="<?=$data[COL_POSTTITLE]?>"
                        data-gallery="gallery"
                        style="background: url('<?=MY_UPLOADURL.$rheader[0][COL_IMGPATH]?>');
                                background-size: cover;
                                background-repeat: no-repeat;
                                background-position: center;
                                width: 100%;
                                min-height: 300px;
                                cursor: pointer;">
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                }
                ?>
              </div>
              <!-- News Title -->
              <h1 class="news-title"><?=$data[COL_POSTTITLE]?></h1>
              <!-- Meta -->
              <div class="meta">
                <div class="meta-left">
                  <span class="author"><i class="far fa-user"></i>&nbsp;<?=$data[COL_NAME]?></span>
                  <span class="author"><i class="far fa-clock"></i>&nbsp;<?=date('d-m-Y H:i', strtotime($data[COL_CREATEDON]))?></span>
                </div>
                <div class="meta-right" style="margin-top: 0 !important">
                  <span class="views"><i class="fa fa-eye"></i><?=number_format($data[COL_TOTALVIEW])?> kali dilihat</span>
                </div>
              </div>
              <!-- News Text -->
              <div class="news-text"><?=$postContent?></div>
            </div>
          </div>
          <div class="col-12">
            <div class="comments-form">
              <div id="disqus_thread"></div>
              <script>

                  /**
                   *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                   *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                  /*
                   */
                  var disqus_config = function () {
                      this.page.url = '<?=site_url('site/home/page/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                      this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                  };
                  (function() { // DON'T EDIT BELOW THIS LINE
                      var d = document, s = d.createElement('script');
                      s.src = '<?=$this->setting_web_disqus_url?>';
                      s.setAttribute('data-timestamp', +new Date());
                      (d.head || d.body).appendChild(s);
                  })();
              </script>
              <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-12">
        <div class="main-sidebar">
          <!-- Single Widget -->
          <div class="single-widget search">
            <div class="form">
              <input type="email" placeholder="Pencarian">
              <a class="button" href="#"><i class="fa fa-search"></i></a>
            </div>
          </div>
          <!--/ End Single Widget -->

          <!-- Single Widget -->
          <div class="single-widget recent-post">
            <h3 class="title">Berita Terkini</h3>
            <?php
            $n=0;
            foreach($berita as $b) {
              $n++;
              $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
              $img = $this->db->where(COL_ISTHUMBNAIL, 1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
              ?>
              <div class="single-post">
                <div class="image">
                  <div style="
                  height: 80px;
                  width: 80px;
                  float: left;
                  background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                  background-size: cover;
                  background-repeat: no-repeat;
                  background-position: center;
                  ">
                  </div>
                </div>
                <div class="content">
                  <h5><a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>"><?=$b[COL_POSTTITLE]?></a></h5>
                  <ul class="comment">
                    <li><i class="fa fa-calendar" aria-hidden="true"></i><?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></li>
                    <li><i class="fa fa-eye" aria-hidden="true"></i><?=number_format($b[COL_TOTALVIEW])?></li>
                  </ul>
                </div>
              </div>

              <?php
            }
            ?>
          </div>
          <!--/ End Single Widget -->
        </div>
      </div>
    </div>
  </div>
</section>
