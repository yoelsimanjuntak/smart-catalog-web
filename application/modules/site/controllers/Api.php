<?php
class Api extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
  }

  public function login() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    $data = json_decode($json);

    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $username = $this->input->post('username');
    $password = $this->input->post('password');

    if($this->muser->authenticate($username, $password)) {
      if($this->muser->IsSuspend($username)) {
        ShowJsonError('Akun anda di suspend.');
        return;
      }

      $this->db->where(COL_USERNAME, $username);
      $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

      $userdetails = $this->muser->getdetails($username);
      //$token = GetEncryption($userdetails[COL_USERNAME]);
      ShowJsonSuccess('Login berhasil.', array('data'=>array(
        'Email' => $userdetails[COL_EMAIL],
        'Role' => $userdetails[COL_ROLENAME],
        'ID_Role' => $userdetails[COL_ROLEID],
        'Nama' => $userdetails[COL_NAME],
        'Address' => $userdetails[COL_ADDRESS]
        //'Token' => $token)));
      )));
    } else {
      ShowJsonError('Username / password tidak tepat.');
    }
  }

  public function register() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    $data = json_decode($json);

    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $userdata = array(
      COL_USERNAME => $this->input->post('phoneno'),
      COL_PASSWORD => md5($this->input->post('password')),
      COL_ROLEID => 2,
      COL_ISSUSPEND => false
    );
    $userinfo = array(
      COL_USERNAME => $this->input->post('phoneno'),
      COL_EMAIL => $this->input->post('phoneno'),
      COL_NAME => $this->input->post('name'),
      COL_ADDRESS => $this->input->post('address'),
      COL_PHONENUMBER => $this->input->post('phoneno'),
      COL_REGISTEREDDATE => date('Y-m-d')
    );

    $reg = $this->muser->register($userdata, $userinfo, null);
    if(!$reg) {
      ShowJsonError('Registrasi gagal!');
      exit();
    }

    ShowJsonSuccess('Registrasi berhasil.');
    exit();
  }

  public function posts($cat) {
    $this->load->model('mpost');

    $resp = array();
    $res = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->where(TBL__POSTS.'.'.COL_POSTCATEGORYID, $cat)
    ->where(COL_ISSUSPEND, false)
    ->order_by(COL_CREATEDON,'desc')
    ->get(TBL__POSTS, 5)
    ->result_array();

    foreach($res as $r) {
      $rthumb = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $r[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
      $resp[] = array(
        COL_POSTID=>$r[COL_POSTID],
        COL_POSTTITLE=>$r[COL_POSTTITLE],
        COL_POSTCATEGORYNAME=>$r[COL_POSTCATEGORYNAME],
        COL_POSTPRICE=>$r[COL_POSTPRICE],
        'Thumbnail'=>!empty($rthumb)?MY_UPLOADURL.$rthumb[COL_IMGPATH]:null,
        COL_POSTDATE=>date('d-m-Y', strtotime($r[COL_CREATEDON]))
      );
    }
    ShowJsonSuccess('Loaded.', array('data'=>$resp));
  }

  public function products($cat) {
    $this->load->model('mpost');

    $resp = array();
    $res = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->where(TBL__POSTS.'.'.COL_POSTCATEGORYID, $cat)
    ->where(COL_ISSUSPEND, false)
    ->order_by(COL_POSTTITLE,'asc')
    ->get(TBL__POSTS)
    ->result_array();

    foreach($res as $r) {
      $rthumb = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $r[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
      $resp[] = array(
        COL_POSTID=>$r[COL_POSTID],
        COL_POSTCATEGORYID=>$r[COL_POSTCATEGORYID],
        COL_POSTTITLE=>$r[COL_POSTTITLE],
        COL_POSTCATEGORYNAME=>$r[COL_POSTCATEGORYNAME],
        COL_POSTPRICE=>$r[COL_POSTPRICE],
        'Thumbnail'=>!empty($rthumb)?MY_UPLOADURL.$rthumb[COL_IMGPATH]:null,
        COL_POSTDATE=>date('d-m-Y', strtotime($r[COL_CREATEDON]))
      );
    }
    ShowJsonSuccess('Loaded.', array('data'=>$resp));
  }

  public function products_featured($cat) {
    $this->load->model('mpost');

    $resp = array();
    $res = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->where(TBL__POSTS.'.'.COL_POSTCATEGORYID, $cat)
    ->where(COL_ISSUSPEND, false)
    ->order_by(COL_CREATEDON,'desc')
    ->get(TBL__POSTS, 5)
    ->result_array();

    foreach($res as $r) {
      $rthumb = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $r[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
      $resp[] = array(
        COL_POSTID=>$r[COL_POSTID],
        COL_POSTCATEGORYID=>$r[COL_POSTCATEGORYID],
        COL_POSTTITLE=>$r[COL_POSTTITLE],
        COL_POSTCATEGORYNAME=>$r[COL_POSTCATEGORYNAME],
        COL_POSTPRICE=>$r[COL_POSTPRICE],
        'Thumbnail'=>!empty($rthumb)?MY_UPLOADURL.$rthumb[COL_IMGPATH]:null,
        COL_POSTDATE=>date('d-m-Y', strtotime($r[COL_CREATEDON]))
      );
    }
    ShowJsonSuccess('Loaded.', array('data'=>$resp));
  }

  public function product($id) {
    $resp = array();
    $res = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->where(TBL__POSTS.'.'.COL_POSTID, $id)
    ->get(TBL__POSTS)
    ->row_array();

    if($res) {
      $rthumb = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $res[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
      $resp = array(
        COL_POSTID=>$res[COL_POSTID],
        COL_POSTCATEGORYID=>$res[COL_POSTCATEGORYID],
        COL_POSTTITLE=>$res[COL_POSTTITLE],
        COL_POSTCATEGORYNAME=>$res[COL_POSTCATEGORYNAME],
        COL_POSTPRICE=>$res[COL_POSTPRICE],
        'Thumbnail'=>!empty($rthumb)?MY_UPLOADURL.$rthumb[COL_IMGPATH]:null,
        COL_POSTDATE=>date('d-m-Y', strtotime($res[COL_CREATEDON])),
        COL_POSTCONTENT=>$res[COL_POSTCONTENT]
      );
    }
    ShowJsonSuccess('Loaded.', array('data'=>$resp));
  }

  public function order_add() {
    $dat = array(
      COL_ORDERNAME=>$this->input->post(COL_ORDERNAME),
      COL_ORDERPHONE=>$this->input->post(COL_ORDERPHONE),
      COL_ORDERADDRESS=>$this->input->post(COL_ORDERADDRESS),
      COL_ORDERSTATUS=>$this->input->post(COL_ORDERSTATUS),
      COL_ORDERTOTAL=>toNum($this->input->post(COL_ORDERTOTAL)),
      COL_ORDERITEMS=>$this->input->post(COL_ORDERITEMS),
      COL_ORDERREMARKS=>$this->input->post(COL_ORDERREMARKS),
      COL_USERNAME=>$this->input->post(COL_USERNAME),
      COL_CREATEDON=>date('Y-m-d H:i:s')
    );
    $rdata = array();

    $id='';
    $this->db->trans_begin();
    try {
      $res = $this->db->insert(TBL_ORDERS, $dat);
      if(!$res) {
        throw new Exception('Terjadi kesalahan pada server.');
      }

      $id = $this->db->insert_id();
      $this->db->trans_commit();

      $rdata = $this->db
      ->where(COL_ORDERID, $id)
      ->get(TBL_ORDERS)
      ->row_array();

    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }
    ShowJsonSuccess('Pesanan No. <strong>'.str_pad($id,4,"0",STR_PAD_LEFT).'</strong> berhasil ditambahkan.', array('data'=>$rdata));
  }

  public function orders() {
    $uname = $this->input->post(COL_USERNAME);
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      ShowJsonError('Autentikasi gagal.');
      exit();
    }

    $res = $this->db
    ->join(TBL_ORDERSTATUS,TBL_ORDERSTATUS.'.'.COL_STATUSNAME." = ".TBL_ORDERS.".".COL_ORDERSTATUS,"inner")
    ->where(COL_USERNAME, $uname)
    ->order_by(TBL_ORDERSTATUS.'.'.COL_STATUSSEQ, 'asc')
    ->order_by(TBL_ORDERS.'.'.COL_CREATEDON, 'desc')
    ->get(TBL_ORDERS)
    ->result_array();
    ShowJsonSuccess('Loaded.', array('data'=>$res));
  }

  public function order($id) {
    $resp = array();
    $res = $this->db
    ->where(TBL_ORDERS.'.'.COL_ORDERID, $id)
    ->get(TBL_ORDERS)
    ->row_array();

    if($res) {
      $res = array_merge($res, array('BankInfo'=>GetSetting('SETTING_ORG_FAX')));
    }
    ShowJsonSuccess('Loaded.', array('data'=>$res));
  }
}
