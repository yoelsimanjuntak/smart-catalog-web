<?php
class Order extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  public function index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      show_error('ANDA TIDAK MEMILIKI AKSES!');
      exit();
    }
    $data['title'] = "Pesanan";
    $this->template->load('backend', 'order/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $sessPkg = !empty($_POST['filterPackage'])?$_POST['filterPackage']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_ORDERID,COL_ORDERNAME,COL_ORDERSTATUS,null,COL_CREATEDON);
    $cols = array(COL_ORDERID, COL_ORDERNAME, COL_ORDERSTATUS);

    $queryAll = $this->db
    ->get(TBL_ORDERS);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($sessPkg)) {
      $this->db->where(COL_ORDERSTATUS, $sessPkg);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->join(TBL_ORDERSTATUS,TBL_ORDERSTATUS.'.'.COL_STATUSNAME." = ".TBL_ORDERS.".".COL_ORDERSTATUS,"inner")
    ->order_by(TBL_ORDERSTATUS.'.'.COL_STATUSSEQ, 'asc')
    ->get_compiled_select(TBL_ORDERS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/order/info/'.$r[COL_ORDERID]).'" class="btn btn-xs btn-outline-primary"><i class="fas fa-info-circle"></i>&nbsp;RINCIAN</a>&nbsp;';
      $data[] = array(
        $htmlBtn,
        str_pad($r[COL_ORDERID],4,"0",STR_PAD_LEFT),
        $r[COL_ORDERNAME],
        $r[COL_ORDERSTATUS],
        number_format($r[COL_ORDERTOTAL]),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function info($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      show_error('ANDA TIDAK MEMILIKI AKSES!');
      exit();
    }
    $data['title'] = "Rincian";

    $rdata = $this->db
    ->where(COL_ORDERID, $id)
    ->get(TBL_ORDERS)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $data['title'] = "Pesanan #".str_pad($rdata[COL_ORDERID],4,"0",STR_PAD_LEFT);
    $data['data'] = $rdata;

    if(!empty($_POST)) {
      $res = $this->db
      ->where(COL_ORDERID, $id)
      ->update(TBL_ORDERS, array(COL_ORDERSTATUS=>$this->input->post(COL_ORDERSTATUS)));
      redirect(current_url());
    } else {
      $this->template->load('backend', 'order/info', $data);
    }
  }
}
?>
